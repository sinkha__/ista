<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MenuMakanan extends Model
{
    use HasFactory;
    protected $table = 'menu_makanans';
    protected $primaryKey = 'menukd';
    public $incrementing = false;
    protected $fillable = [
        'menukd',
        'menunm',
        'stok',
        'hargajual'
    ];
}
