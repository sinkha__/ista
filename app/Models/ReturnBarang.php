<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Cabang;
use App\Models\SupplyBarang;

class ReturnBarang extends Model
{
    use HasFactory;
    protected $table = 'returns';
    protected $primaryKey = 'rkode';
    public $incrementing = false;
    protected $fillable = [
        'rkode',
        'cabkode',
        'bkode',
        'rtgl',
        'rjml',
        'rharga'
    ];

    public function cabang()
    {
        return $this->belongsTo(Cabang::class, 'cabkode');
    }

    public function supplybarang(){
        return $this->belongsTo(SupplyBarang::class, 'bkode');
    }
}
