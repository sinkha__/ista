<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Transaksi extends Model
{
    use HasFactory;
    protected $table = 'transaksis';
    protected $primaryKey = 'jkode';
    public $incrementing = false;
    protected $fillable = [
        'cabkode',
        'jkode',
        'jmeja',
        'tgl',
        'totalbayar'
    ];

    public function cabang()
    {
        return $this->belongsTo(Cabang::class, 'cabkode');
    }

    public function menumakanan()
    {
        return $this->belongsTo(MenuMakanan::class, 'menukd');
    }

    public function detail_transaksi()
    {
        return $this->belongsTo(detail_transaksi::class);
    }
}
