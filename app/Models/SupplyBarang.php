<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Supplier;

class SupplyBarang extends Model
{
    use HasFactory;
    protected $table = 'supply_barangs';
    protected $primaryKey = 'bkode';
    public $incrementing = false;
    protected $fillable = [
        'bkode',
        'bnama',
        'btgl',
        'bjumlah',
        'hargabeli',
        'supkode'
    ];

    public function supplier()
    {
        return $this->belongsTo(Supplier::class, 'supkode');
    }

    public function return()
    {
      return $this->hasMany(ReturnBarang::class, 'bkode');
    }
}
