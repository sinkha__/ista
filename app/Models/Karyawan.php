<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Bagian;

class Karyawan extends Model
{
    use HasFactory;
    protected $table = 'karyawans';
    protected $primaryKey = 'karnik';
    public $incrementing = false;
    protected $fillable = [
        'karnik',
        'karnama',
        'karjk',
        'kartl',
        'karal',
        'kartelp',
        'bagid',  
        'kartgljoin'   
    ];

    public function bagian()
    {
        return $this->belongsTo(Bagian::class, 'bagid');
    }
}
