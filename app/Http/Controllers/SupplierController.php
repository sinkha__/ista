<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Supplier;
use PDF;
use RealRashid\SweetAlert\Facades\Alert;


class SupplierController extends Controller
{

    public function index()
    {
        $supplier = Supplier::latest()->get();
    
        return view('admin.supplier.index',compact('supplier'));  
    }

    public function cetak_pdf()
    {
        // return 'iii';
    	$supplier = Supplier::all();
        // return view('admin/supplier/sup_pdf',['supplier'=>$supplier]);
    	$pdf = PDF::loadview('admin/supplier/sup_pdf',['supplier'=>$supplier]);
    	return $pdf->download('daftar-supplier.pdf');
    }

    public function create()
    {
        return view('admin.supplier.create');

    }

    
    public function store(Request $request)
    {
        $request->validate([
            'supkode' => 'required',
            'supnama' => 'required',
        ]);
     
        $cek = Supplier::find($request['supkode']);
        if($cek == ""){
        Supplier::create($request->all());
            Alert::success('Sukses', 'Data Supplier Berhasil Ditambahkan');
            return redirect()->route('supplier.index');
        }else{
            Alert::error('Gagal', 'Data Supplier Gagal Ditambahkan');
            return redirect()->route('supplier.index');
        }
        
                    
    }

   
    public function show($id)
    {
        //
    }

    public function search(Request $request)
    {
        $search = $request->get('search');
        $supplier = Supplier::latest()->paginate(5);
        return view('admin.supplier.index',compact('supplier'))
        ->with('i', (request()->input('page', 1) - 1) * 5);
    }

    
    public function edit($supkode)
    {
        $supplier = Supplier::findOrFail($supkode);
        return view('admin.supplier.edit',compact('supplier'));
    }

    
    public function update(Request $request, Supplier $supplier)
    {
        $request->validate([
            'supnama' => 'required',
        ]);
    
        $supplier->update($request->all());
    
        Alert::success('Sukses', 'Data Supplier Berhasil Diubah');
            return redirect()->route('supplier.index');
    }

   
    public function destroy(Supplier $supplier)
    {
        $supplier->delete();
    
        Alert::success('Sukses', 'Data Supplier Berhasil Dihapus');
            return redirect()->route('supplier.index');
    }

   
}
