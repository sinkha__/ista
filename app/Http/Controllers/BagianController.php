<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
// use Illuminate\Support\Facades\DB;
use App\Models\Bagian;

class BagianController extends Controller
{
    public function index()
    {
        $bagian = Bagian::latest()->paginate(5);
    
        return view('admin.bagian.index',compact('bagian'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }

    public function create()
    {
        return view('admin.bagian.create');
    }

    public function store(Request $request)
    {
         $request->validate([
            'bagnama' => 'required',
            'bagpok' => 'required',
        ]);
    
        Bagian::create($request->all());
     
        return redirect()->route('bagian.index')
                        ->with('success','Bagian created successfully.');
    }


    public function show(Bagian $bagian)
    {
       //
    }


    public function edit($bagid)
    {
        $bagian = Bagian::findOrFail($bagid);
        return view('admin.bagian.edit',compact('bagian'));
    }

    public function update(Request $request, Bagian $bagian)
    {
        $request->validate([
            'bagnama' => 'required',
            'bagpok' => 'required',
        ]);
    
        $bagian->update($request->all());
    
        return redirect()->route('bagian.index')
                        ->with('success','Bagian updated successfully');
    }

    public function destroy(Bagian $bagian)
    {
        $bagian->delete();
    
        return redirect()->route('bagian.index')
                        ->with('success','Bagian deleted successfully');
    }

   
}
