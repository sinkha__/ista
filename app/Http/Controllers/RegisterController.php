<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class RegisterController extends Controller
{
    public function create()
    {
        return view('register.create');
    }

    public function store()
    {
        $this->validate(request(), [
            'name' => 'required',
            'username' => 'required',
            'email' => 'required|email',
            'password' => 'required',
            'level' => 'required'
        ]);

        $users = User::create(request(['name','username','email','password','level']));
        // auth()->login($users);
        return redirect()->to('akun');
    }
}
