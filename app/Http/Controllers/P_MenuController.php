<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\MenuMakanan;

class P_MenuController extends Controller
{
    public function index()
    {
        $menumakanan = MenuMakanan::latest()->get();
    
        return view('pemilik.p_menumakanan.index',compact('menumakanan'));
    }
}
