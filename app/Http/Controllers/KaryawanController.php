<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Karyawan;
use App\Models\Bagian;


class KaryawanController extends Controller
{
    
    public function adminkar()
    {
        $karyawan = Karyawan::latest()->paginate(5);
    
        return view('admin.karyawan.index',compact('karyawan'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }

    public function index()
    {
        return view('karyawan.karyawan');
    }

    
    public function create()
    {
        $bagian = Bagian::all();
        return view('admin.karyawan.create',compact('bagian'));
    }


    public function store(Request $request)
    {
        $request->validate([
            'karnik' => 'required',
            'karnama' => 'required',
            'karjk' => 'required',
            'kartl' => 'required',
            'karal' => 'required',
            'kartelp' => 'required',
            'bagid' => 'required',
            'kartgljoin' => 'required',
        ]);
    
        Karyawan::create($request->all());
     
        return redirect()->route('karyawan.index')
                        ->with('success','Karyawan created successfully.');
    }


    public function show($karnik)
    {
        //
    }


    public function edit($karnik)
    {
        $bagian = Bagian::all();
        $karyawan =Karyawan::findOrFail($karnik);
        return view('admin.karyawan.edit',compact('karyawan','bagian'));
    }

    public function update(Request $request, Karyawan $karyawan)
    {
        $request->validate([
            'karnik' => 'required',
            'karnama' => 'required',
            'karjk' => 'required',
            'kartl' => 'required',
            'karal' => 'required',
            'kartelp' => 'required',
            'bagid' => 'required',
            'kartgljoin' => 'required',
        ]);

        $karyawan->update($request->all());
    
        return redirect()->route('karyawan.index')
                        ->with('success','Karyawan updated successfully');

    }


    public function destroy(Karyawan $karyawan)
    {
        $karyawan->delete();
    
        return redirect()->route('karyawan.index')
                        ->with('success','Karyawan deleted successfully');

    }
}
