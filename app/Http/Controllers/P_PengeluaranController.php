<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\SupplyBarang;
use App\Models\Kerusakan;

class P_PengeluaranController extends Controller
{
    public function index()
    {
        $supplybarang = SupplyBarang::latest()->get();

        $sumsupply = $supplybarang->sum('hargabeli');
    
        return view('pemilik.lp_keluar.index',compact('supplybarang', 'sumsupply'));
    }

}
