<?php

namespace App\Http\Controllers;

use App\Models\Transaksi;
use App\Models\detail_transaksi;
use App\Models\Cabang;
use App\Models\MenuMakanan;
use Illuminate\Http\Request;
use PDF;


class TransaksiController extends Controller
{
    
    public function index()
    {
        $transaksi = Transaksi::leftjoin('detail_transaksis as dt', 'dt.jkode', '=', 'transaksis.jkode')
        ->selectRaw("transaksis.jkode, tgl, totalbayar, count(jumlah) as jml")
        ->groupBy("jkode", "tgl", "totalbayar")
        ->orderBy("tgl", "desc")
        ->get();
        // return $transaksi;
        return view('kasir.transaksi.index',compact('transaksi'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $cabang = Cabang::all();
        $menumakanan = MenuMakanan::all();
        return view('kasir.transaksi.create',compact('cabang', 'menumakanan'));
    }

    public function store(Request $request)
    {
        $transaksi = new Transaksi;
        $transaksi->cabkode = $request->cabkode;
        $transaksi->jkode = "tr-".date('YmdHis');
        $transaksi->jmeja = $request->jmeja;
        $transaksi->tgl = date('Y-m-d') ;
        $transaksi->totalbayar = $request->totalbayar;
        $transaksi->save();

        foreach ($request->menumakanan as $key => $value) {
            $detail_transaksi = new detail_transaksi;
            $detail_transaksi->jkode = "tr-".date('YmdHis');
            $detail_transaksi->menukd = $request->k_menumakanan[$key];
            $detail_transaksi->jumlah = $request->jumlah[$key];
            $detail_transaksi->hargajum = $request->hargajum[$key];
            $detail_transaksi->save();

            $stokPerMenuKD = MenuMakanan::where('menukd',$request->k_menumakanan[$key])
                                        ->get()->first()->stok;
            $new_stok = $stokPerMenuKD - $request->jumlah[$key];
            MenuMakanan::where('menukd',$request->k_menumakanan[$key])
                                        ->update(['stok' => $new_stok]);

        }

        // return $request->all();
        foreach ($request->k_menumakanan as $key => $value) {
            $data_tr[$key]['k_menumakanan'] = $request->k_menumakanan[$key];
            $data_tr[$key]['hargajual'] = $request->hargajual[$key];
            $data_tr[$key]['jumlah'] = $request->jumlah[$key];
            $data_tr[$key]['hargajum'] = $request->hargajum[$key];
            $data_tr[$key]['kode_tr'] = $request->kode_tr[$key];
            $data_tr[$key]['menumakanan'] = $request->menumakanan[$key];
            $data_tr[$key]['tgl'] = $request->tgl[$key];
        }
        // return $data_tr;
        $data['detail_tr'] = $data_tr;
        $data['cabkode'] = $request->cabkode;
        $data['jmeja'] = $request->jmeja;
        $data['totalbayar'] = $request->totalbayar;
        $data['bayar'] = $request->bayar;
        $data['kembalian'] = $request->kembalian;
        $data['tgl'] = $request->tgl[$key];
        // return $data;

        // return view('kasir/transaksi/nota', $data);
    	$pdf = PDF::loadview('kasir/transaksi/nota',$data);
    	return $pdf->download($request->kode_tr[$key].'.pdf');                
        // return 'ok';

    }

    public function cetak_pdf()
    {
        
    	// $transaksi = Transaksi::all();
        return view('kasir/transaksi/nota');
    	// $pdf = PDF::loadview('kasir/transaksi/nota',['transaksi'=>$transaksi]);
    	// return $pdf->download('daftar-penjualan.pdf');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Transaksi  $transaksi
     * @return \Illuminate\Http\Response
     */
    public function show(Transaksi $transaksi)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Transaksi  $transaksi
     * @return \Illuminate\Http\Response
     */
    public function edit(Transaksi $transaksi)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Transaksi  $transaksi
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Transaksi $transaksi)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Transaksi  $transaksi
     * @return \Illuminate\Http\Response
     */
    public function destroy(Transaksi $transaksi)
    {
        //
    }
}
