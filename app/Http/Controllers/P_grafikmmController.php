<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use DB;

class P_grafikmmController extends Controller
{
    public function index(Request $request)
    {
      // $date = Carbon::now();
        if ($request->ajax()) {
          // dd($request->all());
          $date = Carbon::createFromFormat('Y-m-d', $request->date);
          $tahun = $date->year;
          $bulan = $date->month;
          $tanggal = $date->day;
          $nama = [];
          $data = [];
        } else {
          $tahun = Carbon::now()->year;
          $bulan = Carbon::now()->month;
          $tanggal = Carbon::now()->day;
          $nama = [];
          $data = [];
        }

        if ($request->ajax()) {
          $data_rekap_raw = DB::select("SELECT menunm, SUM(jumlah) AS rekap FROM
            (SELECT dt.`menukd`, menunm, jumlah, dt.created_at FROM detail_transaksis dt
            JOIN menu_makanans mkn ON mkn.menukd = dt.menukd
            WHERE MONTH(dt.created_at) = ?
            AND YEAR(dt.created_at) = ?
            AND DAY(dt.created_at) = ? )data_raw
            GROUP BY menunm
            ORDER BY SUM(jumlah) DESC
            LIMIT 5",[$bulan,$tahun,$tanggal]); 
        } else {
          $data_rekap_raw = DB::select("SELECT menunm, SUM(jumlah) AS rekap FROM
            (SELECT dt.`menukd`, menunm, jumlah, dt.created_at FROM detail_transaksis dt
            JOIN menu_makanans mkn ON mkn.menukd = dt.menukd)data_raw
            GROUP BY menunm
            ORDER BY SUM(jumlah) DESC
            LIMIT 5"); 
        }
        
        foreach ($data_rekap_raw as $key => $value) {
            $data[$key]['name'] = $value->menunm;
            $data[$key]['data'] = [$value->rekap];

            $nama[$key] = $value->menunm;
        }

        $send['data_rekap'] = $data;
        $send['data_nama'] = $nama;

        if ($request->ajax()) {
          return view('partials.grafikmm', $send);
        } else {
          return view('pemilik.grafikmm.index', $send);
        }

        // return $data;
        // return $nama;
    }
}
