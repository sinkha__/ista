<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ReturnBarang;
use App\Models\Cabang;
use App\Models\SupplyBarang;
use PDF;
use Carbon\Carbon;
use RealRashid\SweetAlert\Facades\Alert;

class ReturnBarangController extends Controller
{
   
    public function index()
    {
        $returnbarang = ReturnBarang::with('supplybarang')->latest()->get();
    
        return view('admin.returnbarang.index',compact('returnbarang'));
    }

    public function cetak_pdf()
    {
    	$returnbarang = ReturnBarang::all();
    	$pdf = PDF::loadview('admin/returnbarang/k_pdf',['returnbarang'=>$returnbarang]);
    	return $pdf->download('daftar-returnbarang.pdf');
    }
    
    public function create()
    {
        $cabang = Cabang::all();
        $barang = SupplyBarang::whereDate('created_at', Carbon::now())->get();
        // dd($barang);
        return view('admin.returnbarang.create',compact('cabang', 'barang'));
    }

    
    public function store(Request $request)
    {
        // dd($request->all());
        $request->validate([
            'rkode' => 'required',
            'cabkode' => 'required',
            'bkode' => 'required',
            'rtgl' => 'required',
            'rjml' => 'required',
            'rharga' => 'required',
        ]);

        $cek = ReturnBarang::find($request['rkode']);
        if($cek == ""){
            $barang = SupplyBarang::where('bkode', $request->bkode)->first();
            $hargabaru = $barang->hargabeli - $request->rharga;
            $stokbaru = $barang->bjumlah - $request->rjml;

            $barang->hargabeli = $hargabaru;
            $barang->bjumlah = $stokbaru;
            $barang->save();
            // dd($barang);
            ReturnBarang::create($request->except('_token'));
            Alert::success('Sukses', 'Data Return Barang Berhasil Ditambahkan');
            return redirect()->route('returnbarang.index');
        }else{
            Alert::error('Gagal', 'Data Return Barang Gagal Ditambahkan');
            return redirect()->route('returnbarang.index');
        }
    }

   
    public function show($id)
    {
        //
    }

    
    public function edit($rkode)
    {
        $cabang = Cabang::all();
        $returnbarang = ReturnBarang::findOrFail($rkode);
        $barang = SupplyBarang::whereHas('return')->get();
        return view('admin.returnbarang.edit',compact('returnbarang','cabang', 'barang'));

    }

    
    public function update(Request $request, ReturnBarang $returnbarang)
    {
      // dd($request->all());
        $request->validate([
            'rkode' => 'required',
            'cabkode' => 'required',
            'bkode' => 'required',
            'rtgl' => 'required',
            'rjml' => 'required',
            'rharga' => 'required',
        ]);

        $barang = SupplyBarang::where('bkode', $request->bkode)->first();
        $return = ReturnBarang::where('bkode', $request->bkode)->first();
        $hargalama = $barang->hargabeli + $return->rharga;
        $hargabaru = $hargalama - $request->rharga;
        $stoklama = $barang->bjumlah + $return->rjml;
        $stokbaru = $stoklama - $request->rjml;
        $barang->hargabeli = $hargabaru;
        $barang->bjumlah = $stokbaru;
        $barang->save();
        $returnbarang->update($request->all());
        return redirect()->route('returnbarang.index')
                        ->with('success','Data Return Barang Berhasil diubah.');

    }

    
    public function destroy($id)
    {
        //
    }
}
