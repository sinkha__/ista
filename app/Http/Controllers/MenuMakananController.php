<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\MenuMakanan;
use RealRashid\SweetAlert\Facades\Alert;


class MenuMakananController extends Controller
{
    
    public function index()
    {
        $menumakanan = MenuMakanan::latest()->get();
    
        return view('admin.menumakanan.index',compact('menumakanan'));
    }

    public function cetak_pdf(){
        $menumakanan = MenuMakanan::all();
    	$pdf = PDF::loadview('admin/menumakanan/menu_pdf',['menumakanan'=>$menumakanan]);
    	return $pdf->download('daftar-menumakanan.pdf');
    }

    public function create()
    {
         return view('admin.menumakanan.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'menukd' => 'required',
            'menunm' => 'required',
            'stok' => 'required',
            'hargajual' => 'required',
        ]);

        $cek = MenuMakanan::find($request['menukd']);
        if($cek == ""){
        MenuMakanan::create($request->all());
        Alert::success('Sukses', 'Data Menu Makanan Berhasil ditambahkan');
            return redirect()->route('menumakanan.index');
        }else{
            Alert::error('Gagal', 'Data Menu Makanan Gagal Ditambahkan');
            return redirect()->route('menumakanan.index');
        }
    }

    public function byKategori(Request $request){
        $menu = MenuMakanan::where('kategori', $request->kategori)
        ->where('menunm', 'like', '%' . $request->q . '%')
        ->get();

        // dd($menu);

        return view('partials.menubykategori', compact('menu'));
    }


    public function show($id)
    {
        //
    }


    public function edit($menukd)
    {
        // $supplybarang = SupplyBarang::all();
        $menumakanan = MenuMakanan::findOrFail($menukd);
        return view('admin.menumakanan.edit',compact('menumakanan'));

        // return view('admin.menumakanan.edit',compact('menumakanan','supplybarang'));
    }


    public function update(Request $request, MenuMakanan $menumakanan)
    {
        $request->validate([
            'menukd' => 'required',
            'menunm' => 'required',
            'stok' => 'required',
            'hargajual' => 'required',
        ]);

        $menumakanan->update($request->all());

        return redirect()->route('menumakanan.index')
                        ->with('success','Menu makanan berhasil diubah.');

    }


    public function destroy(MenuMakanan $menumakanan)
    {
        $menumakanan->delete();
    
        return redirect()->route('menumakanan.index')
                        ->with('success','Data Menu Makanan Berhasil dihapus deleted successfully');

    }
}