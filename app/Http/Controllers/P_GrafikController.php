<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Models\Cabang;
use App\Models\Transaksi;
use Carbon\Carbon;

class P_GrafikController extends Controller
{

    public function index(Request $request)
    {

        $jumlah = 12;
        $tahun = Transaksi::selectRaw("YEAR(tgl) as tahun")->where('tgl', '!=', null)->groupBy('tahun')->get();
        $tahunnow = Transaksi::selectRaw("YEAR(tgl) as tahun")->whereYear('tgl', Carbon::now()->year)->groupBy('tahun')->first();
        
        // dd($tahun);
        // $tahun = (isset($request->tahun))? $request->tahun : 2021;
        $cabang = Cabang::all();

        foreach ($cabang as $key => $value) {
            $data[$key]['name'] = $value->cabnama;
            
            for ($i=0; $i < $jumlah; $i++) {
    
                $data_rekap_raw = DB::select("SELECT a.cabkode, MONTH(a.tgl) AS bulan, YEAR(a.tgl) AS tahun, SUM(a.totalbayar) AS total FROM
                                            (SELECT * FROM transaksis)a
                                            WHERE YEAR(a.tgl) = ?
                                            AND MONTH(tgl) = ?
                                            AND cabkode = ?
                                            GROUP BY MONTH(tgl), YEAR(a.tgl), cabkode
                                            ",[$tahunnow->tahun, $i+1, $value->cabkode]);
                // return $data_rekap_raw[0]->total;
                $total[$i] = (isset($data_rekap_raw[0]->total)) ? $data_rekap_raw[0]->total : 0;
            }
            $data[$key]['data'] = $total;

        }
    
        // return $data;
        $send['data_rekap'] = $data;
        return view('pemilik.grafik.index', $send, compact('tahun'));  
    }
    
    public function getDataGrafikPendapata(Request $request)
    {
        $jumlah = 12;
        $tahun = (isset($request->tahun))? $request->tahun : date('Y');
        $cabang = Cabang::all();

        foreach ($cabang as $key => $value) {
            $data[$key]['name'] = $value->cabnama;
            
            for ($i=0; $i < $jumlah; $i++) {
    
                $data_rekap_raw = DB::select("SELECT a.cabkode, MONTH(a.tgl) AS bulan, YEAR(a.tgl) AS tahun, SUM(a.totalbayar) AS total FROM
                                            (SELECT * FROM transaksis)a
                                            WHERE YEAR(a.tgl) = ?
                                            AND MONTH(tgl) = ?
                                            AND cabkode = ?
                                            GROUP BY MONTH(tgl), YEAR(a.tgl), cabkode
                                            ",[$tahun, $i+1, $value->cabkode]);
                // return $data_rekap_raw[0]->total;
                $total[$i] = (isset($data_rekap_raw[0]->total)) ? $data_rekap_raw[0]->total : 0;
            }
            $data[$key]['data'] = $total;

        }
    
        // return $data;
        $send = $data;
        return json_encode($send);
    }

    
}
