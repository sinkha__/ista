<?php

namespace App\Http\Controllers;

use App\Models\c_kasir;
use Illuminate\Http\Request;

class CKasirController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\c_kasir  $c_kasir
     * @return \Illuminate\Http\Response
     */
    public function show(c_kasir $c_kasir)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\c_kasir  $c_kasir
     * @return \Illuminate\Http\Response
     */
    public function edit(c_kasir $c_kasir)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\c_kasir  $c_kasir
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, c_kasir $c_kasir)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\c_kasir  $c_kasir
     * @return \Illuminate\Http\Response
     */
    public function destroy(c_kasir $c_kasir)
    {
        //
    }
}
