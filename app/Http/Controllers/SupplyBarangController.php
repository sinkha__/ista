<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\SupplyBarang;
use App\Models\Supplier;
use PDF;
use RealRashid\SweetAlert\Facades\Alert;

class SupplyBarangController extends Controller
{
    
    public function index()
    {
        $supplybarang = SupplyBarang::latest()->get();
    
        return view('admin.supplybarang.index',compact('supplybarang'));
    }

    public function cetak_pdf()
    {
    	$supplybarang = SupplyBarang::all();
    	$pdf = PDF::loadview('admin/supplybarang/sb_pdf',['supplybarang'=>$supplybarang]);
    	return $pdf->download('daftar-barang.pdf');
    }

    public function create()
    {
        $supplier = Supplier::all();
        return view('admin.supplybarang.create',compact('supplier'));
    }

    
    public function store(Request $request)
    {
        $request->validate([
            'bkode' => 'required',
            'bnama' => 'required',
            'btgl' => 'required',
            'bjumlah' => 'required',
            'hargabeli' => 'required',
            'supkode' => 'required',
        ]);
    
        $cek = SupplyBarang::find($request['bkode']);
        if($cek == ""){
        SupplyBarang::create($request->all());
        Alert::success('Sukses', 'Data Supply Barang Berhasil ditambahkan');
            return redirect()->route('supplybarang.index');
        }else{
            Alert::error('Gagal', 'Data Supply Barang Gagal Ditambahkan');
            return redirect()->route('supplybarang.index');
        }
        
    }

   
    public function show($id)
    {
        //
    }

    
    public function edit($bkode)
    {
        $supplier = Supplier::all();
        $supplybarang = SupplyBarang::findOrFail($bkode);
        return view('admin.supplybarang.edit',compact('supplybarang','supplier'));
    }

    
    public function update(Request $request, SupplyBarang $supplybarang)
    {
        $request->validate([
            'bkode' => 'required',
            'bnama' => 'required',
            'btgl' => 'required',
            'bjumlah' => 'required',
            'hargabeli' => 'required',
            'supkode' => 'required',
        ]);

        $supplybarang->update($request->all());

        return redirect()->route('supplybarang.index')
                        ->with('success','Supply Barang Berhasil diubah.');
    }

    
    public function destroy($id)
    {
        //
    }
}
