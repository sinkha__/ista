<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Supplier;

class P_SupController extends Controller
{
    public function index()
    {
        $supplier = Supplier::latest()->get();
    
        return view('pemilik.p_supplier.index',compact('supplier'));  
    }
}
