<?php

namespace App\Http\Controllers;

use App\Models\Transaksi;
use App\Models\detail_transaksi;
use App\Models\Cabang;
use App\Models\MenuMakanan;
use Illuminate\Http\Request;
use Carbon\Carbon;
use DB;

class P_PemasukanController extends Controller
{
    public function index(Request $request,$cabang,$bulan,$tahun)
    {
        $transaksi = Transaksi::leftjoin('detail_transaksis as dt', 'dt.jkode', '=', 'transaksis.jkode')
        ->selectRaw("transaksis.jkode, tgl, totalbayar, count(jumlah) as jml")
               
        ->when($cabang, function ($query) use ($cabang) {
            if($cabang != 'all') {
                return $query->where('cabkode','=', $cabang);
            }else{
                return $query;
                    }})
        ->when($bulan, function ($query) use ($bulan) {
            if($bulan != 'all') {
                return $query->where(DB::raw("month(transaksis.tgl)"),'=', $bulan);
            }else{
                return $query;
                    }})
        ->when($tahun, function ($query) use ($tahun) {
            if($tahun != 'all') {
                return $query->where(DB::raw("year(transaksis.tgl)"),'=', $tahun);
            }else{
                return $query;
                    }})
        // ->where("cabkode",'like', "'%".$pcabang."%'")
        ->groupBy("jkode", "tgl", "totalbayar")
        
        ->orderBy("tgl", "desc")
        ->get();

        $sum = $transaksi->sum('totalbayar');
        
        // return $transaksi;
        return view('pemilik.pemasukan.index',compact('transaksi','cabang','bulan','tahun', 'sum'));
    }
    public function getDataTahun(Request $request){
        $jumlah = 12;
        $tahun = (isset($request->tahun))? $request->tahun : date('Y');
        $cabang = Cabang::all();
    }
}
