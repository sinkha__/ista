<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\SupplierController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('login');
});

// LOGIN
Route::get('login','App\Http\Controllers\AuthController@index')->name('login');
Route::post('proses_login','App\Http\Controllers\AuthController@proses_login')->name('proses_login');

Route::get('logout', 'App\Http\Controllers\AuthController@logout')->name('logout');

Route::group(['middleware' => ['auth']], function () {
    Route::group(['middleware' => ['cek_login:admin']], function () {
        Route::get('admin','App\Http\Controllers\AdminController@index')->name('admin');
        Route::get('adminkar', 'App\Http\Controllers\KaryawanController@adminkar')->name('adminkar');
        Route::resource('datakar', 'App\Http\Controllers\KaryawanController');
    }); 
    Route::group(['middleware' => ['cek_login:pemilik']], function () {
        Route::get('pemilik','App\Http\Controllers\PemilikController@index')->name('pemilik');
    }); 
    Route::group(['middleware' => ['cek_login:karyawan']], function () {
        Route::get('/karyawan', function () {
            return view('karyawan.karyawan');
        })->name('karyawan');
    }); 
    Route::group(['middleware' => ['cek_login:kasir']], function () {
        Route::get('kasir','App\Http\Controllers\KasirController@index')->name('kasir');
    });   
});

//REGISTER
Route::get('register', 'App\Http\Controllers\RegisterController@create')->name('register');
Route::post('register', 'App\Http\Controllers\RegisterController@store')->name('register');

Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return view('dashboard');
})->name('dashboard');

Route::resource('bagian', 'App\Http\Controllers\BagianController');

//  AKUN
Route::get('akun', 'App\Http\Controllers\AkunController@index')->name('akun');

//SUPPLIER
Route::resource('supplier', 'App\Http\Controllers\SupplierController');
Route::get('/cetak_pdf/supplier', 'App\Http\Controllers\SupplierController@cetak_pdf');

//SUPPLYBARANG
Route::resource('supplybarang', 'App\Http\Controllers\SupplyBarangController');
Route::get('/cetak_pdf/supplybarang', 'App\Http\Controllers\SupplyBarangController@cetak_pdf');

//KERUSAKAN
Route::resource('returnbarang', 'App\Http\Controllers\ReturnBarangController');
Route::get('/cetak_pdf/returnbarang', 'App\Http\Controllers\ReturnBarangController@cetak_pdf');

// //MENU
Route::get('/menu/get', 'App\Http\Controllers\MenuMakananController@byKategori')->name('menu.bykategori');
Route::resource('menumakanan', 'App\Http\Controllers\MenuMakananController');
Route::get('/cetak_pdf/menumakanan', 'App\Http\Controllers\MenuMakananController@cetak_pdf');

//KASIR
Route::resource('/transaksi', 'App\Http\Controllers\TransaksiController');
Route::get('/cetak_pdf/transaksi', 'App\Http\Controllers\TransaksiController@cetak_pdf');

// //ABSENSI
Route::resource('/absensi', 'App\Http\Controllers\AbsensiController');

//PEMILIK
Route::resource('/p_supplier', 'App\Http\Controllers\P_SupController');
Route::resource('/p_menumakanan', 'App\Http\Controllers\P_MenuController');
// Route::resource('/pemasukan', 'App\Http\Controllers\P_PemasukanController');
Route::get('/pemasukan/{cabang}/{bulan}/{tahun}', 'App\Http\Controllers\P_PemasukanController@index')->name('getPemasukan');
Route::resource('/pengeluaran', 'App\Http\Controllers\P_PengeluaranController');

Route::get('/grafik/{tahun}', 'App\Http\Controllers\P_GrafikController@index');
Route::get('/grafikMakananFavorit', 'App\Http\Controllers\P_grafikmmController@index')->name('grafikmm');
Route::get('/getDataGrafikPendapata ', 'App\Http\Controllers\P_GrafikController@getDataGrafikPendapata')->name('getDataGrafikPendapata');


