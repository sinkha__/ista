<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;

class AkunSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = [
            [
            'username' => 'admin',
            'name' => 'indah sindi',
            'email' => 'admin@admin.com',
            'level' => 'admin',
            'password' => bcrypt('admin123')
            ],
            [
                'username' => 'pemilik',
                'name' => 'piaway',
                'email' => 'pemilik@pemilik.com',
                'level' => 'pemilik',
                'password' => bcrypt('pemilik123')
            ],
            [
                'username' => 'karyawan',
                'name' => 'eko',
                'email' => 'karyawan@karyawan.com',
                'level' => 'karyawan',
                'password' => bcrypt('karyawan123')
            ],
            [
                'username' => 'kasir',
                'name' => 'oke',
                'email' => 'kasir@kasir.com',
                'level' => 'kasir',
                'password' => bcrypt('kasir123')
            ]
            ];
            foreach ($user as $key => $value) {
                User::create($value);

            }
    }
}
