<figure class="highcharts-figure">
  <div id="container"></div>
  <p class="highcharts-description">
      
  </p>
</figure>

<script>
  var data_rekap = {!!json_encode($data_rekap)!!};
  var data_nama = {!!json_encode($data_nama)!!};

  Highcharts.chart('container', {
  chart: {
      type: 'column'
  },
  title: {
      text: 'Grafik Menu Terfavorit '
  },
  subtitle: {
      text: 'Angkringan Panjer Wengi'
  },
  xAxis: {
      categories: ['nama'],
      crosshair: true
  },
  yAxis: {
      min: 0,
      title: {
          text: 'Total'
      }
  },
  tooltip: {
      headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
      pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name} </td>' +
          '<td style="padding:0"><b>{point.y:.1f} </b></td></tr>',
      footerFormat: '</table>',
      shared: true,
      useHTML: true
  },
  plotOptions: {
      column: {
          pointPadding: 0.2,
          borderWidth: 0
      }
  },
  series:data_rekap
});
</script>