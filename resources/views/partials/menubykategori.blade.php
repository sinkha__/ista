<hr>
<div class="row">
    @forelse($menu as $i)
    <div class="col-md-4">
        <div class="card card-body">
            <p><strong>{{$i->menunm}}</strong></p>
            @if($i->stok <= 0)
            <p>Stok Habis!</p>
            @else
            <p id="stok{{$i->menukd}}" data-stok="{{$i->stok}}">Stok <strong>{{$i->stok}}</strong></p>
            <hr>
            <!-- <strong>Jumlah</strong> -->
            <!-- <input type="number" class="form-control"> -->
            <div class="row">
                <div class="col-6">
                    <input type="number" id="jumlah{{$i->menukd}}" class="form-control" value="0">
                </div>
                <div class="col-6">
                    <button class="btn btn-primary btn-sm" data-id="{{$i->menukd}}" id="btn_tambahkeranjang{{$i->menukd}}"><i class="fas fa-plus"></i> Keranjang</button>
                </div>
            </div>
            @endif
        </div>
    </div>

    <script>
    $('#btn_tambahkeranjang{{$i->menukd}}').on('click', function (e) {
        var sisastok = parseInt($('#stok{{$i->menukd}}').data('stok')) - parseInt($('#jumlah{{$i->menukd}}').val());
        console.log($('#jumlah{{$i->menukd}}').val());
        // if (sisastok < 0) {
            
        // } else {
            
        // }
        e.preventDefault();
        jumlah = ($('#jumlah{{$i->menukd}}').val() != '') ? $('#jumlah{{$i->menukd}}').val() : 0;
        menumakanan = "{{$i->menunm}}";
        k_menumakanan = "{{$i->menukd}}";
        hargajual = "{{$i->hargajual}}";
        hargajum = jumlah*hargajual;

        if($('#stok{{$i->menukd}}').data('stok') < jumlah){
            alert('Stok kurang!');
        } else {
            $('#stok{{$i->menukd}}').data('stok', sisastok);
            $('#stok{{$i->menukd}}').html('Stok <strong>' + sisastok + '</strong>');
        if (jumlah == 0) {  
            alert('jumlah tidak boleh 0');
            return false;
        }else if (menumakanan == 'Pilih Menu') {
            alert('Silahkan pilih menu terlebih dahulu')
            return false;
        } 
        if(if_add == 0){
            var content =  '<tr class="baris" id="baris{{$i->menukd}}" data-id="{{$i->menukd}}"><td><div class="form-group"><input type="text" name="kode_tr[]" id="kode_tr{{$i->menukd}}" data-id="{{$i->menukd}}" value="'+id+'" class="form-control-plaintext" placeholder=""></div></td>'
                content += '<td><div class="form-group"><input type="text" name="menumakanan[]" id="t_menumakanan{{$i->menukd}}" data-id="{{$i->menukd}}" class="form-control-plaintext" value="'+menumakanan+'" placeholder="">'
                content += '<input hidden type="text" name="k_menumakanan[]" id="k_menumakanan{{$i->menukd}}" data-id="{{$i->menukd}}" class="form-control-plaintext" value="'+k_menumakanan+'" placeholder=""></div></td>'
                content += '<td><div class="form-group"><input type="text" name="tgl[]" id="t_tgl{{$i->menukd}}" data-id="{{$i->menukd}}" class="form-control-plaintext" value="{{date('Y-m-d')}}" placeholder=""></div></td>'
                content += '<td><div class="form-group"><input type="text" name="jumlah[]" id="t_jumlah{{$i->menukd}}" data-id="{{$i->menukd}}" class="form-control-plaintext" value="'+jumlah+'" placeholder=""></div></td>'
                content += '<td><div class="form-group"><input type="text" name="hargajual[]" id="t_hargajual{{$i->menukd}}" data-id="{{$i->menukd}}" class="form-control-plaintext" value="'+hargajual+'" placeholder=""></div></td>'
                content += '<td><div class="form-group"><input type="text" name="hargajum[]" id="t_hargajum{{$i->menukd}}" data-id="{{$i->menukd}}" class="form-control-plaintext" value="'+hargajum+'" placeholder=""></div></td>'
                content += '<td><a href="javascript:void(0)" class="btn btn-danger btn_hapus" id="btnHapus{{$i->menukd}}" data-jumlah="'+jumlah+'" data-id="{{$i->menukd}}">Hapus</a></td></tr>'
            $('#transaksi_content').append(content);
            
        }else{

            $('.baris').each(function() {
                if( "{{$i->menunm}}" == $('#t_menumakanan'+$(this).data('id')).val() ){
                    var t_jumlah =  parseInt($('#t_jumlah'+$(this).data('id')).val()) + parseInt(jumlah);
                    var hargajual = $('#t_hargajual'+$(this).data('id')).val();
                    var t_harga_subtotal = t_jumlah * hargajual;
                    console.log(t_jumlah+' '+t_harga_subtotal+' '+t_harga_subtotal)

                    $('#t_jumlah'+$(this).data('id')).val(t_jumlah);
                    $('#t_jumlah'+$(this).data('id')).val(t_jumlah);
                    $('#btnHapus'+$(this).data('id')).data('jumlah', t_jumlah);
                    console.log('btn'+$('#btnHapus'+$(this).data('id')).data('jumlah'))
                    $('#t_hargajum'+$(this).data('id')).val(t_harga_subtotal);
                    cek = 1;
                    return false;
                }else{
                    cek = 0;
                }
            })

            if(cek == 0){
                console.log(hargajual);
                    
                    var content1 =  '<tr class="baris" id="baris{{$i->menukd}}" data-id="{{$i->menukd}}"><td><div class="form-group"><input type="text" name="kode_tr[]" id="kode_tr{{$i->menukd}}" data-id="{{$i->menukd}}" value="'+id+'" class="form-control-plaintext" placeholder=""></div></td>'
                        content1 += '<td><div class="form-group"><input type="text" name="menumakanan[]" id="t_menumakanan{{$i->menukd}}" data-id="{{$i->menukd}}" class="form-control-plaintext" value="'+menumakanan+'" placeholder="">'
                        content1 += '<input hidden type="text" name="k_menumakanan[]" id="k_menumakanan{{$i->menukd}}" data-id="{{$i->menukd}}" class="form-control-plaintext" value="'+k_menumakanan+'" placeholder=""></div></td>'
                        content1 += '<td><div class="form-group"><input type="text" name="tgl[]" id="t_tgl{{$i->menukd}}" data-id="{{$i->menukd}}" class="form-control-plaintext" value="{{date('Y-m-d')}}" placeholder=""></div></td>'
                        content1 += '<td><div class="form-group"><input type="text" name="jumlah[]" id="t_jumlah{{$i->menukd}}" data-id="{{$i->menukd}}" class="form-control-plaintext" value="'+jumlah+'" placeholder=""></div></td>'
                        content1 += '<td><div class="form-group"><input type="text" name="hargajual[]" id="t_hargajual{{$i->menukd}}" data-id="{{$i->menukd}}" class="form-control-plaintext" value="{{$i->hargajual}}" placeholder=""></div></td>'
                        content1 += '<td><div class="form-group"><input type="text" name="hargajum[]" id="t_hargajum{{$i->menukd}}" data-id="{{$i->menukd}}" class="form-control-plaintext" value="'+hargajum+'" placeholder=""></div></td>'
                        content1 += '<td><a href="javascript:void(0)" class="btn btn-danger btn_hapus" id="btnHapus{{$i->menukd}}" data-jumlah="'+jumlah+'" data-id="{{$i->menukd}}">Hapus</a></td></tr>'
                    $('#transaksi_content').append(content1);        
                    console.log('tambah_1');            
            }
        }

        total_all = 0;
        $('.baris').each(function() {
            var subtotal = parseInt($('#t_hargajum'+$(this).data('id')).val());
            
                total_all = total_all + subtotal;
                console.log(total_all);

            $('#totalbayar').val(total_all);
        })    
        if_add = if_add + 1;
        count = count + 1;
        }
        $('#jumlah{{$i->menukd}}').val(0);
    });
    </script>
    @empty
    @endforelse
</div>