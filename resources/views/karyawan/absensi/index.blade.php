@extends('layout')
@section('title','Halaman Absensi')
@section('header')
<center><h4>Scan QR Code</h4></center>
@endsection

@section('content')
    <center><video width="500px" height="auto" id="preview"></video></center>
    <form action="{{route('absensi.store')}}">
    @csrf
    <input name="abid" type="text" id="absen">
    <button>Absen</button>
    </form>
@endsection

@section('script')
<script src="https://rawgit.com/schmich/instascan-builds/master/instascan.min.js"></script>
<script type="text/javascript">
      let scanner = new Instascan.Scanner({ video: document.getElementById('preview') });
      scanner.addListener('scan', function (content) {
        $("#absen").val(content);
      });
      Instascan.Camera.getCameras().then(function (cameras) {
        if (cameras.length > 0) {
          scanner.start(cameras[0]);
        } else {
          console.error('No cameras found.');
        }
      }).catch(function (e) {
        console.error(e);
      });
    </script>
@endsection
