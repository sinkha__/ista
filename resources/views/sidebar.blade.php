<div class="sidebar sidebar-style-2">
			<div class="sidebar-background"></div>
			<div class="sidebar-wrapper scrollbar scrollbar-inner">
				<div class="sidebar-content">
				@if(Auth::user()->level == 'admin')
				<!-- admin -->
					<ul class="nav nav-info">
						<li class="nav-item active">
							<a href="#">
								<i class="fas fa-home"></i>
								<p>Admin : {{Auth::user()->name}}</p>
							</a>
						</li>
						<li class="nav-item">
							<a href="{{ route('admin') }}">
								<p>Home</p>
							</a>
						</li>
						</li><li class="nav-item">
							<a href="{{ route('supplier.index') }}">
								<p>Supplier</p>
							</a>
						</li>
						<li class="nav-item">
							<a href="{{ route('supplybarang.index') }}">
								<p>Supply Barang</p>
							</a>
						</li>
						<li class="nav-item">
							<a href="{{ route('returnbarang.index') }}">
								<p>Return Barang</p>
							</a>
						</li>
						<li class="nav-item">
							<a href="{{ route('menumakanan.index') }}">
								<p>Kelola Menu Makanan</p>
							</a>
						</li>
						<!-- <li class="nav-item dropdown">
							<a class="nav-link nav-dropdown-toggle" href="#">Kelola Karyawan</a>
							<ul class="nav-dropdown-items">
								<li clas="nav-item"><a class="nav-link" href="{{ route('adminkar') }}">Data Karyawan</a></li>
								<li clas="nav-item"><a class="nav-link" href="#">Absensi</a></li>
								<li clas="nav-item"><a class="nav-link" href="{{ route('bagian.index') }}">Bagian</a></li>
							</ul>
						</li> -->
						<!-- <li class="nav-item dropdown">
							<a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								Dropdown link
							</a>
							<div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
								<a class="dropdown-item" href="#">Action</a>
								<a class="dropdown-item" href="#">Another action</a>
								<a class="dropdown-item" href="#">Something else here</a>
							</div>
						</li>
						<li class="nav-item">
							<a href="components/alerts.html">
								<p>Kelola Gaji</p>
							</a>
						</li>
						<li class="nav-item">
							<a href="{{ route('akun') }}">
								<p>Kelola Akun</p>
							</a>
						</li>
					</ul> -->
					@elseif(Auth::user()->level == 'pemilik')
					<!-- pemilik -->
					<ul class="nav nav-info">
						<li class="nav-item active">
							<a href="#">
								<i class="fas fa-home"></i>
								<p>Pemilik : {{Auth::user()->name}}</p>
							</a>
						</li>
						<li class="nav-item">
							<a href="{{ route('pemilik') }}">
								<p>Home</p>
							</a>
						</li>
						<!-- <li class="nav-item">
							<a href="components/alerts.html">
								<p>Kinerja Karyawan</p>
							</a>
						</li> -->
						<li class="nav-item">
							<a href="{{ route('p_supplier.index') }}">
								<p>Supplier</p>
							</a>
						</li>
						<li class="nav-item">
							<a href="{{ route('p_menumakanan.index') }}">
								<p>Menu Makanan</p>
							</a>
						</li>
						<li class="nav-item nav-dropdown">
							<a class="nav-link nav-dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							Laporan
							</a>
							<div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
								<!-- <li class="nav-item"><a class="nav-link" href="#">Laporan Gaji</a></li> -->
								<a class="dropdown-item" href="{{ route('getPemasukan',['all','all','all']) }}">Laporan Pemasukan</a>
								<a class="dropdown-item" href="{{ route('pengeluaran.index') }}">Laporan Pengeluaran</a>
								<a class="dropdown-item" href="{{ url('grafik').'/'.date('Y')}}">Grafik Pendapatan</a>
								<a class="dropdown-item" href="{{  url('grafikMakananFavorit')  }}">Grafik Menu Favorit</a>
							</div>
							
						</li>
					</ul>
					@elseif(Auth::user()->level == 'kasir')
					<!-- kasir -->
					<ul class="nav nav-info">
						<li class="nav-item active">
							<a href="#">
								<i class="fas fa-home"></i>
								<p>Kasir : {{Auth::user()->name}} </p>
							</a>
						</li>
						<li class="nav-item">
							<a href="{{ route('kasir') }}">
								<p>Home</p>
							</a>
						</li>
						<li class="nav-item">
							<a href="{{ route('transaksi.create') }}">
								<p>Transaksi Penjualan</p>
							</a>
						</li>
						<li class="nav-item">
							<a href="{{ route('transaksi.index') }}">
								<p>Rekap</p>
							</a>
						</li>
					</ul>
					@else
					<!-- karyawan -->
					<ul class="nav nav-info">
						<li class="nav-item active">
							<a href="#">
								<i class="fas fa-home"></i>
								<p>Karyawan</p>
							</a>
						</li>
						<li class="nav-item">
							<a href="{{ route('karyawan') }}">
								<p>Home</p>
							</a>
						</li>
						<li class="nav-item">
							<a href="{{ route('absensi.index') }}">
								<p>Absensi</p>
							</a>
						</li>
						<li class="nav-item">
							<a href="components/alerts.html">
								<p>Gaji</p>
							</a>
						</li>
					</ul>
					@endif
				</div>
			</div>
		</div>