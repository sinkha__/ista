@extends('layout')
@section('title','Halaman Kasir')
@section('header')
<center><h4>Daftar Transaksi Penjualan</h4></center>
@endsection

@section('content')

@if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif

    <table id = "datatrans" class="table table-borderes table-striped table-hover">
        <thead>
            <tr>
                <th scope="col">No</th>
                <th scope="col">Kode</th>
                <th scope="col">Tanggal</th>
                <th scope="col">Jumlah</th>
                <th scope="col">Total Pembayaran</th>
            </tr>
        </thead>
        <tbody>            
            @php $i=1 @endphp
            @foreach ($transaksi as $trans)
                <tr>
                    <td>{{ $i++ }}</td>
                    <td>{{ $trans['jkode'] }}</td>
                    <td>{{ $trans['tgl'] }}</td>
                    <td>{{ $trans['jml'] }}</td>
                    <td>{{ $trans['totalbayar'] }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>  
@endsection

@push('script')
<script>
$(document).ready(function () {
    $('#datatrans').dataTable();
});
</script>
@endpush