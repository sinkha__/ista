<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <style type="text/css">
        body {
			font-family: "Times New Roman", Times, serif;
			font-size: 12pt;
		}

		.text-center {
			text-align: center;
		}

		.text-left {
			text-align: left;
		}

		.text-right {
			text-align: right;
		}

		.text-uppercase {
			text-transform: uppercase;
		}

		.text-lowercase {
			text-transform: lowercase;
		}

		.text-capital {
			text-transform: capitalize;
		}

		.text-underline {
			text-decoration: underline;
			text-decoration-color: #000;
		}

		.font-sm {
			font-size: 12px;
		}

		.bg-red {
			background-color: red;
		}

		.bg-grey {
			background-color: rgb(220, 220, 220);
		}

		.table {
			border-collapse: collapse;
			border-spacing: 0;
			width: 100%;
			border: solid 1px black;
		}

		.table th,
		.table td {
			border: 1px solid black;
			font-size: 12px;
			padding: 5px;
		}

		.mb-0 {
			margin-bottom: 0px;
		}

		.mt-0 {
			margin-top: 0px;
		}

		.my-0 {
			margin-bottom: 0px;
			margin-top: 0px;
		}

		.mb-1 {
			margin-bottom: 1.5px;
		}

		.mar {
			margin-top: 10px;
			margin-bottom: 10px
		}

		hr {
			display: block;
			margin-top: 0.3em;
			margin-bottom: -0.2em;
			margin-left: auto;
			margin-right: auto;
			border-style: inset;
			border-width: 3px;
			background: black;
		}

		ol {
			display: block;
			margin-top: 0em;
			margin-bottom: 1em;
			margin-left: 0;
			margin-right: 0;
			padding-left: 17px;
			padding-top: -15px;
		}
    </style>
        <center>
            <h3>Nota Penjualan</h3>
            <h3>Angkringan Panjer Wengi</h3>
			<p>Jl. Raya Kediri - No. 56, Kertosari, Kandat, Kec. Kandat, Kediri, Jawa Timur </p>
			<p> FOLLOW US ON INSTAGRAM : @angkringan_panjerwengi</p>
        </center>
        <table class ='table'>
            <tr>
            <td class="text-left" style="border : 1px solid white;">Tanggal : {{$tgl}}</td>
            <td class="text-right" style="border : 1px solid white;">No. Meja : {{$jmeja}}</td>
            </tr>
        </table>
        

        <table class='table table-bordered'>
            <thead>
                <tr>
                    <th>No</th>
                    <th>Nama Menu</th>
                    <th>Jumlah</th>
                    <th>Harga</th>
                    <th>Total</th>
                </tr>
            </thead>
            <tbody>
                @php $i=1 @endphp
                
                @foreach($detail_tr as $dt)
                <tr>
                    <td>{{ $i++ }}</td>
                    <td>{{ $dt['menumakanan'] }}</td>
                    <td>{{ $dt['jumlah'] }}</td>
                    <td>{{ $dt['hargajual'] }}</td>
                    <td>{{ $dt['hargajum'] }}</td>
                </tr>
                @endforeach
                <tr>
                    <td style="border : 1px solid white;"></td>
                    <td style="border : 1px solid white;"></td>
                    <td style="border-left : 1px solid white; border-bottom : 1px solid white; "></td>
                    <td class="text-right" style="border : 1px solid black; ">Total</td>
                    <td>{{ $totalbayar }}</td>
                </tr>
                <tr>
                <td style="border : 1px solid white;"></td>
                    <td style="border : 1px solid white;"></td>
                    <td style="border-left : 1px solid white; border-bottom : 1px solid white; "></td>
                    <td class="text-right" style="border : 1px solid black; ">Bayar</td>
                    <td>{{ $bayar }}</td>
                </tr>
                <tr>
                <td style="border : 1px solid white;"></td>
                    <td style="border : 1px solid white;"></td>
                    <td style="border-left : 1px solid white; border-bottom : 1px solid white;  "></td>
                    <td class="text-right" style="border : 1px solid black; ">Kembalian</td>
                    <td>{{ $kembalian }}</td>
                </tr>
            </tbody>
            
        </table>
		<center>
			<p>Kritik & Saran : </p>
			<p>WA : +62 852 3204 9663</p>
			<p>Terima kasih </p>
		</center>
</body>
</html>