@extends('layout')
@section('title','Halaman Kasir')
@section('header')
<center><h4>Tambah Transaksi Penjualan</h4></center>
@endsection

@section('content')

@if ($errors->any())
    <div class="alert alert-danger">
        <strong>Whoops!</strong> There were some problems with your input.<br><br>
    </div>
@endif

<form action="{{ route('transaksi.store') }}" method="POST" enctype="multipart/form-data">
    @csrf
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <strong>Kode Cabang</strong>
                @php
                    $sesi = Session::get('data_user');

                @endphp
                <select name="cabkode_x" disabled id="cabang" class="form-control">
                    <option value="">Pilih Cabang</option>
                    @foreach ($cabang as $data)
                        <option {{($sesi['cabangkode'] == $data -> cabkode) ? 'selected' : ''}} value="{{ $data -> cabkode  }}">{{ $data-> cabnama }}</option>
                    @endforeach
                    <input name="cabkode" hidden type="text" value="{{$sesi['cabangkode']}}">
                </select>            
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <strong>Nomor Meja</strong>
                <input type="text" name="jmeja" id="jmeja" class="form-control" placeholder="">
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
            <strong>Kategori Makanan</strong>
            <select id="kategorimakanan" class="form-control">
                <option value="" selected disabled hidden>Pilih Kategori</option>
                <option value="0">Minuman</option>
                <option value="1">Makanan</option>
            </select>         
            </div>   
        </div>
        <script>
            $('#kategorimakanan').on('change', function(){
                $.ajax({
                    url: "{{route('menu.bykategori')}}",
                    data: {
                        kategori: $(this).val()
                    },
                    success: function(data){
                        $('#formsearch').removeClass('d-none');
                        console.log(data);
                        $('#menuWrapper').html(data);
                    }
                }) 
            })
        </script>
    </div>
   <br>
   <hr>
    <div class="d-none" id="formsearch">
        <input type="search" id="q" placeholder="Cari menu..." class="form-control">
    <!-- <button id="btnSearch" href="#" class="btn btn-primary">Search</button> -->
    </div>
    <script>
    $('#q').on("keyup", function(e){
        // e.preventDefault();
        $.ajax({
            url : "{{route('menu.bykategori')}}",
            data : {
                kategori: $('#kategorimakanan').val(),
                q: $('#q').val(),
            },
            success: function(data){
                $('#menuWrapper').html(data);
            }
        })
    })
    </script>
   <div id="menuWrapper"></div>


    <div class="table-responsive">
        <table class="table table-bordered table-hover">
            <thead style="background:#dedede">
                <tr>
                    <th>Kode</th>
                    <th>Menu</th>
                    <th>Tanggal</th>
                    <th>Jumlah</th>
                    <th>Harga</th>
                    <th>Total</th>
                    <th>Aksi</th>
                </tr>
            </thead>
            <tbody id="transaksi_content">
            
            </tbody>
        </table> 
    </div>

<div class = "row">
    <div class="col-md-12">
        <div class="form-group">
            <strong>Total Bayar </strong>
            <input type="text" name="totalbayar" id="totalbayar" readonly class="form-control" placeholder="">
         </div>
    </div> 
    <div class="col-md-6">
        <div class="form-group">
            <strong>Bayar</strong>
            <input type="text" name="bayar" id="bayar" class="form-control" placeholder="">
         </div>
    </div> 
    <div class="col-md-6">
        <div class="form-group">
            <strong>Kembali</strong>
            <input type="text" name="kembalian" id="kembalian" readonly class="form-control" placeholder="">
         </div>
    </div> 
    <div class="col-xs-12 col-sm-12 col-md-12 text-center">
        <button type="submit" class="btn btn-primary" id="btn_simpany">Simpan dan Cetak </button>
    </div>
</div>
</form>    
@endsection

@push('script')
<script>
var id = 'tr-{{date('YmdHis')}}';
var count = 1;
var if_add = 0;
var jumlah = 0;
var menumakanan = '';
var hargajual = 0;
var hargajum = 0;
var cek = 0;
var total_all = 0;
$(document).ready(function () {
    // console.log('ok');

});

$('#menumakanan').change(function (e) { 
    var harga = $('#menumakanan :checked').data('harga');
    // console.log($('#menumakanan :checked').data('harga'));
    $('#hargajual').val(harga);
    total();
});
var selisih_bayar = 0;

function pembayaran() {
    var totalbayar = $('#totalbayar').val();
    var bayar = $('#bayar').val();
    selisih_bayar = bayar - totalbayar;
    // console.log(pembayaran);
    if (selisih_bayar < 0) {
        alert("Nominal yang dimasukkan kurang");
    }else{
    var bayar = $('#kembalian').val(selisih_bayar);
    }
}

$('#bayar').keyup(function (e) { 
    // pembayaran();        
});


$(document).on('blur','#bayar',function (e) { 
    e.preventDefault();
    pembayaran(); 
});

$('#jumlah').keyup(function (e) { 
    total();        
});

// function total() {
//     var harga = $('#hargajual').val();
//     var jumlah = $('#jumlah').val();
//     var total = harga * jumlah;
//     // console.log(total);
//     var jumlah = $('#hargajum').val(total);
// }

// $('.btn_hapus').click(function (e) { 
$(document).on('click','.btn_hapus',function (e) {
    e.preventDefault();
    var id = $(this).data('id');
    var harga_jum = $('#t_hargajum'+$(this).data('id')).val();    
    var total_bayar = $('#totalbayar').val();
    var total_new = total_bayar - harga_jum;
    // console.log(total_bayar+total_new+harga_jum);
    var newstok = $(this).data('jumlah') + $('#stok'+$(this).data('id')).data('stok');
    $('#stok'+$(this).data('id')).html('Stok <strong>'+newstok+'</strong>');
    $('#stok'+$(this).data('id')).data('stok', newstok);
    console.log($(this).data('jumlah'));
    $('#baris'+id).remove();
    $('#totalbayar').val(total_new);
});
</script>
@endpush