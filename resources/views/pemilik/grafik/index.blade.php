@extends('layout')
@section('title','Halaman Pemilik')
@section('header')
<!-- <center><h4>Grafik</h4></center> -->
@endsection

@section('content')

<div class="form-group">
    <label for="tahun">Pilih Tahun</label>
    <select name="tahun" id="tahun">
    @foreach($tahun as $i)
     <option value="{{$i->tahun}}" {{$i->tahun == \Carbon\Carbon::now()->year ? 'selected' : ''}}>{{$i->tahun}}</option>
    @endforeach
    </select>
    <figure class="highcharts-figure">
        <div id="container"></div>
        <p class="highcharts-description">
            
        </p>
    </figure>
</div>

@endsection

@push('script')
<script>
    var data_rekap = {!!json_encode($data_rekap)!!};

Highcharts.chart('container', {
    chart: {
        type: 'column'
    },
    title: {
        text: 'Grafik Pendapatan'
    },
    subtitle: {
        text: 'Angkringan Panjer Wengi'
    },
    xAxis: {
        categories: [
            'Jan',
            'Feb',
            'Mar',
            'Apr',
            'May',
            'Jun',
            'Jul',
            'Aug',
            'Sep',
            'Oct',
            'Nov',
            'Dec'
        ],
        crosshair: true
    },
    yAxis: {
        min: 0,
        title: {
            text: 'Omset'
        }
    },
    tooltip: {
        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name} : Rp. </td>' +
            '<td style="padding:0"><b>{point.y:.1f} </b></td></tr>',
        footerFormat: '</table>',
        shared: true,
        useHTML: true
    },
    plotOptions: {
        column: {
            pointPadding: 0.2,
            borderWidth: 0
        }
    },
    series:data_rekap
});

$('#tahun').on('change',function (e) { 
    e.preventDefault();
    var nilai = $(this).val();
    // console.log("{{url('getDataGrafikPendapata')}}?tahun="+nilai);
    $.ajax({
        type: "get",
        url: "{{url('getDataGrafikPendapata')}}?tahun="+nilai,
        dataType: "json",
        success: function (data) {
            console.log(data);
            Highcharts.chart('container', {
    chart: {
        type: 'column'
    },
    title: {
        text: 'Grafik Pendapatan'
    },
    subtitle: {
        text: 'Angkringan Panjer Wengi'
    },
    xAxis: {
        categories: [
            'Jan',
            'Feb',
            'Mar',
            'Apr',
            'May',
            'Jun',
            'Jul',
            'Aug',
            'Sep',
            'Oct',
            'Nov',
            'Dec'
        ],
        crosshair: true
    },
    yAxis: {
        min: 0,
        title: {
            text: 'Omset'
        }
    },
    tooltip: {
        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name} : Rp. </td>' +
            '<td style="padding:0"><b>{point.y:.1f} </b></td></tr>',
        footerFormat: '</table>',
        shared: true,
        useHTML: true
    },
    plotOptions: {
        column: {
            pointPadding: 0.2,
            borderWidth: 0
        }
    },
    series:data
});
        }
    });
});
</script>
@endpush
