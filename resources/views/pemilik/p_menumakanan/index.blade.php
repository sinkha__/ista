@extends('layout')
@section('title','Halaman Pemilik')
@section('header')
<center><h4>Daftar Menu Makanan</h4></center>
@endsection
   
@section('content')

   @if ($message = Session::get('success'))
       <div class="alert alert-success">
           <p>{{ $message }}</p>
       </div>
   @endif

<table id = "datamenu" class="table table-borderes table-striped table-hover">
        <thead>
            <tr>
                <th>No</th>
                <th>Kode Menu</th>
                <th>Menu</th>
                <th>Stok</th>
                <th>Harga Jual</th>
            </tr>
        </thead>
        <tbody>
            @php $i=1 @endphp
            @foreach ($menumakanan as $mm)
            <tr>
                <td>{{ $i++ }}</td>
                <td>{{ $mm->menukd }}</td>
                <td>{{ $mm->menunm }}</td>
                <td>{{ $mm->stok }}</td>
                <td>{{ $mm->hargajual }}</td>
            </tr>
            @endforeach
        </tbody>
    </table>
@endsection

    @push('script')
<script>
$(document).ready(function () {
    $('#datamenu').dataTable();
});
</script>
@endpush
    