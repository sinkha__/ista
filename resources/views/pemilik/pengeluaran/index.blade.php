@extends('layout')
@section('title','Halaman Pemilik')
@section('header')
<center><h4>Daftar Barang</h4></center>
@endsection

@section('content')
<hr>
<table id = "datasb" class="table table-borderes table-striped table-hover">
    <thead>
        <tr>
            <th>No</th>
            <th>Kode Barang</th>
            <th>Nama Barang</th>
            <th>Tanggal</th>
            <th>Jumlah</th>
            <th>Harga Beli</th>
            <th>Supplier</th>
        </tr>
    </thead>
    <tbody>
        @php $i=1 @endphp
        @foreach ($supplybarang as $suppb)
        <tr>
            <td>{{ $i++ }}</td>
            <td>{{ $suppb->bkode }}</td>
            <td>{{ $suppb->bnama }}</td>
            <td>{{ $suppb->btgl }}</td>
            <td>{{ $suppb->bjumlah }}</td>
            <td>{{ $suppb->hargabeli }}</td>
            <td>{{ $suppb->supplier->supnama }}</td>
        </tr>
        @endforeach
    </tbody>
</table> 
@endsection

@push('script')
<script>
$(document).ready(function () {
    $('#datasb').dataTable();
});
</script>
@endpush