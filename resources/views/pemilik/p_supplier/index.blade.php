@extends('layout')
@section('title','Halaman Pemilik')
@section('header')
<center><h4>Daftar Supplier</h4></center>
@endsection

@section('content')
   
<table id ="datasup" class="table table-borderes table-striped table-hover">
        <thead>
            <tr>
                <th scope="col">No</th>
                <th scope="col">Kode Supplier</th>
                <th scope="col">Nama Supplier</th>
            </tr>
        </thead>
        <tbody>
            @php $i=1 @endphp
            @foreach ($supplier as $sup)
            <tr>
                <td>{{ $i++ }}</td>
                <td>{{ $sup->supkode }}</td>
                <td>{{ $sup->supnama }}</td>
            </tr>
            @endforeach
        </tbody>
    </table>
@endsection

@push('script')
<script>
$(document).ready(function () {
    $('#datasup').dataTable();
});
</script>
@endpush