@extends('layout')
@section('title','Halaman Pemilik')
@section('header')
<center><h4>Daftar Transaksi Penjualan</h4></center>
@endsection

@section('content')
<div class="row">
    <div class="col-md-3">
        <div class="form-group">
            <label for="cabang">Pilih Cabang</label>
            <select class="form-control" name="cabang" id="cabang">
            <option selected disabled value="">Silahkan Pilih</option>
            <option value="all">Semua</option>
            <option value="CAB-001">Cabang 01</option>
            <option value="CAB-002">Cabang 02</option>
            </select>
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group">
            <label for="bulan">Pilih Bulan</label>
            <select class="form-control" name="bulan" id="bulan">
            <option selected disabled value="">Silahkan Pilih</option>
            <option value="all">Semua</option>
            <option value="1">Januari</option>
            <option value="2">Februari</option>
            <option value="3">Maret</option>
            <option value="4">April</option>
            <option value="5">Mei</option>
            <option value="6">Juni</option>
            <option value="7">Juli</option>
            <option value="8">Agustus</option>
            <option value="9">September</option>
            <option value="10">Oktober</option>
            <option value="11">November</option>
            <option value="12">Desember</option>
            </select>
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group">
                <label>Tahun</label>
                <input type="text" name="tahun" id = "tahun" class="form-control" placeholder="">
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group">
            <label for="tahun">&nbsp;</label><br>
            <button class="btn btn-success btn-sm" name="btnSimpan" id="btnSimpan">Cari</button>
        </div>
    </div>
</div>




@if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif

    <hr>
    <div class="card card-body">
        Rp.{{number_format($sum, 0, ',', '.')}}
    </div>

    <table id = "datatrans" class="table table-borderes table-striped table-hover">
        <thead>
            <tr>
                <th scope="col">No</th>
                <th scope="col">Kode</th>
                <th scope="col">Tanggal</th>
                <th scope="col">Jumlah</th>
                <th scope="col">Total Pembayaran</th>
            </tr>
        </thead>
        <tbody>            
            @php $i=1 @endphp
            @foreach ($transaksi as $trans)
                <tr>
                    <td>{{ $i++ }}</td>
                    <td>{{ $trans['jkode'] }}</td>
                    <td>{{ $trans['tgl'] }}</td>
                    <td>{{ $trans['jml'] }}</td>
                    <td>{{ $trans['totalbayar'] }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>  
  
@endsection

@push('script')
<script>
var url = '';
var cab = '';
var bulan = '';
var tahun = '';

$(document).ready(function () {
    $('#datatrans').dataTable();

    $('#cabang').val('{{$cabang}}');
    $('#bulan').val('{{$bulan}}');
    $('#tahun').val('{{$tahun}}');

    $('#cabang').change(function (e) { 
        e.preventDefault();
        cab = $(this).val();
        url="{{ url('pemasukan') }}/"+cab+'/'+bulan+'/'+tahun;
    });

    $('#bulan').change(function (e) { 
        e.preventDefault();
        bulan = $(this).val();
        url="{{ url('pemasukan') }}/"+cab+'/'+bulan+'/'+tahun;

    });

    $('#tahun').change(function (e) { 
        e.preventDefault();
        tahun = $(this).val();
        url="{{ url('pemasukan') }}/"+cab+'/'+bulan+'/'+tahun;

    });

    $('#btnSimpan').click(function (e) { 
        e.preventDefault();
        window.location.href = url;
    });

});
</script>
@endpush