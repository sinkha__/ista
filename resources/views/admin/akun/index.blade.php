@extends('layout')
@section('title','Halaman Admin')
@section('header')
<center><h3>Daftar Akun</h3></center>
@endsection

@section('content')
<body>
    <a href="{{ route('register') }}" class="btn btn-primary btn-sm">Tambah Akun</a><br>

    <br/>

    <table class="table table-borderes table-striped table-hover">
        <tr>
            <th>Nama</th>
            <th>Username</th>
            <th>Email</th>
            <th>Level</th>
        </tr>
        @foreach($users as $user)
        <tr>
            <td>{{ $user->name }}</td>
            <td>{{ $user->username }}</td>
            <td>{{ $user->email }}</td>
            <td>{{ $user->level }}</td>
        </tr>
        @endforeach
    </table>
</body>
@endsection

