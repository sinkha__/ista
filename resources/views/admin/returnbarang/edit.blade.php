@extends('layout')
@section('title','Halaman Admin')
@section('header')
<center><h4>Edit Data Return Barang</h4></center>
@endsection

@section('content')
@if ($errors->any())
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
        </div>
    @endif

    <form action="{{ route('returnbarang.update', $returnbarang->rkode) }}" method="POST">
    @csrf    
    @method('PUT')

    <div class="row">
        <div class="col-md-4">
            <div class="form-group">
                <strong>Kode</strong>
                <input type="text" readonly name="rkode" class="form-control" value = "{{ $returnbarang->rkode }}">
            </div>
        </div>
        <div class="col-md-4">
        <div class="form-group">
            <strong>Cabang</strong>
            <select name="cabkode" id="cabang" class="form-control">
            <option value="">Pilih Cabang</option>
            @foreach ($cabang as $data)
                <option value="{{ $data -> cabkode  }}">{{ $data-> cabnama }}</option>
                @endforeach
            </select>            
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
            <strong>Nama Barang</strong>
            <input type="hidden" name="bkode" value="{{$returnbarang->bkode}}">
            <select name="bkode" id="barang" class="form-control" disabled>
            <option value="">Pilih Barang</option>
            @foreach($barang as $i)
                <option value="{{$i->bkode}}" {{$i->bkode == $returnbarang->bkode ? 'selected' : ''}} data-harga="{{$i->bkode == $returnbarang->bkode ? $returnbarang->rharga : $i->rharga}}" data-stok="{{$i->bkode == $returnbarang->bkode ? $returnbarang->rjml : $i->rjml}}">{{$i->bnama}}</option>
            @endforeach
            </select> 
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <strong>Tanggal</strong>
                <input type="date" name="rtgl" class="form-control" value = "{{ $returnbarang->rtgl }}">
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <strong>Jumlah</strong>
                <input type="number" required name="rjml" id="rjml" class="form-control" value = "{{ $returnbarang->rjml }}">
            </div>
        </div>

        <div class="col-md-4">
            <div class="form-group">
                <strong>Harga</strong>
                <input type="text" readonly required name="rharga" id="rharga" class="form-control" value = "{{ $returnbarang->rharga }}">
            </div>
        </div>
        
        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    </div>
   
</form>
<script>
  $('#barang').add('#rjml').on('change keyup', function(){
    var option = $('#barang').find(":selected");
    var hargabeli = option.data('harga');
    var jumlah = option.data('stok');
    var hargaperitem = hargabeli / jumlah;
    var total = $('#rjml').val() * hargaperitem;
    $('#rharga').val(total);
  })
</script>
@endsection