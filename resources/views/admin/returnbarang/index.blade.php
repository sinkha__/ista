@extends('layout')
@section('title','Halaman Admin')
@section('header')
<center><h4>Daftar Return Barang</h4></center>
@endsection

@section('content')

<div class="row">
        <div class="container" style="margin-left: 15px;">
                <a class="btn btn-info btn-sm" href="{{ route('returnbarang.create') }}">Tambah Data Return Barang</a>
                <a class="btn btn-danger btn-sm" href="{{ url('/cetak_pdf/returnbarang') }}">Cetak</a>
        </div>
    </div>

    </br>
   
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
   
    <table id="datareturn" class="table table-borderes table-striped table-hover">
        <thead>
            <tr>
                <th>No</th>
                <th>Kode</th>
                <th>Cabang</th>
                <th>Nama Barang</th>
                <th>Tanggal</th>
                <th>Jumlah</th>
                <th>Harga</th>
                <th>AKSI</th>
            </tr>
        </thead>
        <tbody>
        @php $i=1 @endphp
            @foreach ($returnbarang as $rb)
            <tr>
                <td>{{ $i++ }}</td>
                <td>{{ $rb->rkode }}</td>
                <td>{{ $rb->cabang->cabnama }}</td>
                <td>{{ $rb->supplybarang->bnama }}</td>
                <td>{{ $rb->rtgl }}</td>
                <td>{{ $rb->rjml }}</td>
                <td>{{ $rb->rharga }}</td>
                <td>
                <a href="{{ route('returnbarang.edit', $rb->rkode)}}" <button type="submit" class="btn btn-warning btn-sm btn-edit" title="Edit"><i class="fa fa-edit"></i></button></a>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>  
@endsection

@push('script')
<script>
$(document).ready(function () {
    $('#datareturn').dataTable();
});
</script>
@endpush