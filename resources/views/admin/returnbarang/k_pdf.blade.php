<!DOCTYPE html>
<html>
<head>
	<title>Daftar Supplier</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
	<style type="text/css">
		table tr td,
		table tr th{
			font-size: 9pt;
		}
	</style>
	<center>
		<h5>Daftar Kerusakan Barang</h4>
		<h5>Angkringan Panjer Wengi</h4>
	</center>

	<table class='table table-bordered'>
    <thead>
            <tr>
                <th>No</th>
                <th>Kode Kerusakan</th>
                <th>Cabang</th>
                <th>Nama Barang</th>
                <th>Tanggal</th>
                <th>Jumlah</th>
                <th>keterangan</th>
                <th>Harga</th>
            </tr>
        </thead>
        <tbody>
            @php $i=1 @endphp
            @foreach ($kerusakan as $rusk)
            <tr>
                <td>{{ $i++ }}</td>
                <td>{{ $rusk->rusakid }}</td>
                <td>{{ $rusk->cabang->cabnama }}</td>
                <td>{{ $rusk->rusaknama }}</td>
                <td>{{ $rusk->rusaktgl }}</td>
                <td>{{ $rusk->rusakjml }}</td>
                <td>{{ $rusk->keterangan }}</td>
                <td>{{ $rusk->hargapenggantian }}</td>
            </tr>
            @endforeach
        </tbody>
	</table>

</body>
</html>