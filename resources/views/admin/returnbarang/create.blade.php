@extends('layout')
@section('title','Halaman Admin')
@section('header')
<center><h4>Tambah Data Return Barang</h4></center>
@endsection

@section('content')

@if ($errors->any())
    <div class="alert alert-danger">
        <strong>Whoops!</strong> There were some problems with your input.<br><br>
    </div>
@endif
   
<form action="{{ route('returnbarang.store') }}" method="POST">
    @csrf
  
     <div class="row">
        <div class="col-md-4">
            <div class="form-group">
                <strong>Kode</strong>
                <input type="text" name="rkode" class="form-control" placeholder="">
            </div>
        </div>
        <div class="col-md-4">
        <div class="form-group">
            <strong>Cabang</strong>
            <select name="cabkode" id="cabang" class="form-control">
            <option value="">Pilih Cabang</option>
            @foreach ($cabang as $data)
                <option value="{{ $data -> cabkode  }}">{{ $data-> cabnama }}</option>
            @endforeach
            </select>            
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
            <strong>Nama Barang</strong>
            <select name="bkode" id="barang" class="form-control">
            <option value="">Pilih Barang</option>
            @foreach ($barang as $data)
                <option value="{{ $data->bkode  }}" data-harga="{{ $data->hargabeli }}" data-stok="{{ $data->bjumlah }}">{{ $data->bnama }}</option>
            @endforeach
            </select> 
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <strong>Tanggal</strong>
                <input type="date" name="rtgl" max="{{date('Y-m-d')}}" class="form-control" placeholder="">
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <strong>Jumlah</strong>
                <input type="number" name="rjml" id="rjml" value="0" class="form-control" placeholder="">
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <strong>Harga</strong>
                <input type="number" readonly required id="rharga" name="rharga" class="form-control" placeholder="">
            </div>
        </div>
        
        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    </div>
   
</form>
<script>
  $('#barang').add('#rjml').on('change keyup', function(){
    var option = $('#barang').find(":selected");
    var hargabeli = option.data('harga');
    var jumlah = option.data('stok');
    var hargaperitem = hargabeli / jumlah;
    var total = $('#rjml').val() * hargaperitem;
    $('#rharga').val(total);
  })
</script>
@endsection