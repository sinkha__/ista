<!DOCTYPE html>
<html>
<head>
	<title>Daftar Supplier</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
	<style type="text/css">
		table tr td,
		table tr th{
			font-size: 9pt;
		}
	</style>
	<center>
		<h5>Daftar Barang</h4>
		<h5>Angkringan Panjer Wengi</h4>
	</center>

	<table id = "datasb" class="table table-borderes table-striped table-hover">
    <thead>
        <tr>
            <th>No</th>
            <th>Kode Barang</th>
            <th>Nama Barang</th>
            <th>Tanggal</th>
            <th>Jumlah</th>
            <th>Harga Beli</th>
            <th>Supplier</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($supplybarang as $suppb)
        <tr>
            <td>{{ $suppb->bkode }}</td>
            <td>{{ $suppb->bnama }}</td>
            <td>{{ $suppb->btgl }}</td>
            <td>{{ $suppb->bjumlah }}</td>
            <td>{{ $suppb->hargabeli }}</td>
            <td>{{ $suppb->supplier->supnama }}</td>
        </tr>
        @endforeach
    </tbody>
</table> 

</body>
</html>