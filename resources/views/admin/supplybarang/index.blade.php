@extends('layout')
@section('title','Halaman Admin')
@section('header')
<center><h4>Daftar Barang</h4></center>
@endsection

@section('content')

    <div class="row">
        <div class="container" style="margin-left: 15px;">
                <a class="btn btn-info btn-sm" href="{{ route('supplybarang.create') }}">Tambah Barang</a>
                <a class="btn btn-danger btn-sm" href="{{ url('/cetak_pdf/supplybarang') }}">Cetak</a>
        </div>
    </div>

    </br>
   
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
   
<table id = "datasb" class="table table-borderes table-striped table-hover">
    <thead>
        <tr>
            <th>No</th>
            <th>Kode Barang</th>
            <th>Nama Barang</th>
            <th>Tanggal</th>
            <th>Jumlah</th>
            <th>Harga Beli</th>
            <th>Supplier</th>
            <th>AKSI</th>
        </tr>
    </thead>
    <tbody>
        @php $i=1 @endphp
        @foreach ($supplybarang as $suppb)
        <tr>
            <td>{{ $i++ }}</td>
            <td>{{ $suppb->bkode }}</td>
            <td>{{ $suppb->bnama }}</td>
            <td>{{ $suppb->btgl }}</td>
            <td>{{ $suppb->bjumlah }}</td>
            <td>{{ $suppb->hargabeli }}</td>
            <td>{{ $suppb->supplier->supnama }}</td>
            <td>
            <a href="{{ route('supplybarang.edit', $suppb->bkode)}}" <button type="submit" class="btn btn-warning btn-sm btn-edit" title="Edit"><i class="fa fa-edit"></i></button></a>
            </td>
        </tr>
        @endforeach
    </tbody>
</table> 
@endsection

@push('script')
<script>
$(document).ready(function () {
    $('#datasb').dataTable();
});
</script>
@endpush