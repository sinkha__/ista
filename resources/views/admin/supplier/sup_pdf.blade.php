<!DOCTYPE html>
<html>
<head>
	<title>Daftar Supplier</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
	<style type="text/css">
		table tr td,
		table tr th{
			font-size: 9pt;
		}
	</style>
	<center>
		<h5>Daftar Supplier</h4>
		<h5>Angkringan Panjer Wengi</h4>
	</center>

	<table class='table table-bordered'>
		<thead>
			<tr>
				<th>No</th>
				<th>Kode Supplier</th>
				<th>Nama</th>
			</tr>
		</thead>
		<tbody>
			@php $i=1 @endphp
			@foreach($supplier as $sup)
			<tr>
				<td>{{ $i++ }}</td>
				<td>{{ $sup->supkode}}</td>
				<td>{{ $sup->supnama}}</td>
			</tr>
			@endforeach
		</tbody>
	</table>

</body>
</html>