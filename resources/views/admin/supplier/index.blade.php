@extends('layout')
@section('title','Halaman Admin')
@section('header')
<center><h4>Daftar Supplier</h4></center>
@endsection

@section('content')

    <div class="row">
        <div class="container" style="margin-left: 15px;">
                <a class="btn btn-info btn-sm" href="{{ route('supplier.create') }}">Tambah Supplier</a>
                <a class="btn btn-danger btn-sm" href="{{ url('/cetak_pdf/supplier') }}">Cetak</a>
        </div>
    </div>
    
    </br>
   
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif

    
    <table id ="datasup" class="table table-borderes table-striped table-hover">
        <thead>
            <tr>
                <th scope="col">No</th>
                <th scope="col">Kode Supplier</th>
                <th scope="col">Nama Supplier</th>
                <th scope="col">AKSI</th>
            </tr>
        </thead>
        <tbody>
            @php $i=1 @endphp
            @foreach ($supplier as $sup)
            <tr>
                <td>{{ $i++ }}</td>
                <td>{{ $sup->supkode }}</td>
                <td>{{ $sup->supnama }}</td>
                <td>
                        <form action="{{ route('supplier.destroy', $sup->supkode)}}" method="post">
                        <a href="{{ route('supplier.edit', $sup->supkode)}}" <button type="submit" class="btn btn-warning btn-sm btn-edit" title="Edit"><i class="fa fa-edit"></i></button></a>
                        @csrf
                        @method('DELETE')      
                        <button type="submit" class="btn btn-danger btn-sm btn-flat" title="Hapus"><i class="fa fa-trash"></i></button>                    
                        </form>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>  
@endsection

@push('script')
<script>
$(document).ready(function () {
    $('#datasup').dataTable();
});
</script>
@endpush