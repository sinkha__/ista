@extends('layout')
@section('title','Halaman Admin')
@section('header')
<center><h4>Edit Supplier</h4></center>
@endsection

@section('content')
@if ($errors->any())
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
        </div>
    @endif
  
    <form action="{{ route('supplier.update',$supplier->supkode) }}" method="POST">
        @csrf
        @method('PUT')
   
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Kode Supplier:</strong>
                <input type="text" readonly name="kodesup" class="form-control" placeholder=" " value="{{ $supplier->supkode }}">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Nama Supplier:</strong>
                <input type="text" name="supnama" class="form-control" placeholder="" value="{{ $supplier->supnama }}">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    </div>
   
    </form>
@endsection