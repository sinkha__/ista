@extends('layout')
@section('title','Halaman Admin')
@section('header')
<center><h4>Tambah Menu Makanan</h4></center>
@endsection

@section('content')

@if ($errors->any())
    <div class="alert alert-danger">
        <strong>Whoops!</strong> There were some problems with your input.<br><br>
    </div>
@endif
   
<form action="{{ route('menumakanan.store') }}" method="POST">
    @csrf
  
     <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <strong>Kode Menu</strong>
                <input type="text" name="menukd" class="form-control" placeholder="">
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <strong>Nama Menu</strong>
                <input type="text" name="menunm" class="form-control" placeholder="">
            </div>
        </div>
        
        <div class="col-md-6">
            <div class="form-group">
                <strong>Stok</strong>
                <input type="text" name="stok" class="form-control" placeholder="">
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <strong>Harga Jual</strong>
                <input type="text" name="hargajual" class="form-control" placeholder="">
            </div>
        </div>
        
        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    </div>
   
</form>
@endsection