@extends('layout')
@section('title','Halaman Admin')
@section('header')
<center><h4>Daftar Menu Makanan</h4></center>
@endsection

@section('content')

<div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <a class="btn btn-info btn-sm" href="{{ route('menumakanan.create') }}">Tambah Data Menu Makanan</a>
            </div>
        </div>
    </div>

    </br>
   
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
   
    <table id = "datamenu" class="table table-borderes table-striped table-hover">
        <thead>
            <tr>
                <th>No</th>
                <th>Kode Menu</th>
                <th>Menu</th>
                <th>Stok</th>
                <th>Harga Jual</th>
                <!-- <th>Harga Beli</th> -->
                <th>AKSI</th>
            </tr>
        </thead>
        <tbody>
            @php $i=1 @endphp
            @foreach ($menumakanan as $mm)
            <tr>
                <td>{{ $i++ }}</td>
                <td>{{ $mm->menukd }}</td>
                <td>{{ $mm->menunm }}</td>
                <td>{{ $mm->stok }}</td>
                <td>{{ $mm->hargajual }}</td>
                <td>
                    <form action="{{ route('menumakanan.destroy', $mm->menukd )}}" method="post">
                    <a href="{{ route('menumakanan.edit', $mm->menukd )}}" <button type="submit" class="btn btn-warning btn-sm btn-edit" title="Edit"><i class="fa fa-edit"></i></button></a>
                        @csrf
                        @method('DELETE')      
                        <button type="submit" class="btn btn-danger btn-sm btn-flat" title="Hapus"><i class="fa fa-trash"></i></button>
                    </form>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>  
@endsection

@push('script')
<script>
$(document).ready(function () {
    $('#datamenu').dataTable();
});
</script>
@endpush