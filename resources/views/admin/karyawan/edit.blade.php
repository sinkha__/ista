@extends('layout')
@section('title','Halaman Admin')
@section('header')
<center><h4>Edit Karyawan</h4></center>
@endsection

@section('content')
@if ($errors->any())
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
        </div>
    @endif
  
    <form action="{{ route('datakar.update',$karyawan->karnik) }}" method="POST">
        @csrf
        @method('PUT')
   
    <div class="row">
        <div class="col-md-4">
            <div class="form-group">
                <strong>NIK:</strong>
                <input type="text" name="karnik" class="form-control"  value="{{ $karyawan->karnik }}">
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <strong>NAMA:</strong>
                <input type="text" name="karnama" class="form-control"  value="{{ $karyawan->karnama }}">
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <strong>JENIS KELAMIN:</strong>
                <input type="text" name="karjk" class="form-control"  value="{{ $karyawan->karjk }}">
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <strong>TTL:</strong>
                <input type="text" name="kartl" class="form-control"  value="{{ $karyawan->kartl }}">
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <strong>ALAMAT:</strong>
                <input type="text" name="karal" class="form-control" value="{{ $karyawan->karal }}">
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <strong>TELEPON:</strong>
                <input type="text" name="kartelp" class="form-control" value="{{ $karyawan->kartelp }}">
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
            <strong>BAGIAN</strong>
            <select name="bagid" id="bagian" class="form-control">
                <option value="">Pilih Bagian</option>
                @foreach ($bagian as $data)
                    <option value="{{ $data -> bagid  }}">{{ $data-> bagnama }}</option>
                @endforeach
            </select>            
            </div>
        </div>
        <div class=" col-md-4">
            <div class="form-group">
                <strong>TANGGAL JOIN :</strong>
                <input type="text" name="kartgljoin" class="form-control" placeholder="Contoh : 1000000" value="{{ $karyawan->kartgljoin }}">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    </div>
   
    </form>
@endsection