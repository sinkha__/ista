@extends('layout')
@section('title','Halaman Admin')
@section('header')
<center><h4>Daftar Karyawan</h4></center>
@endsection

@section('content')

<div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-right">
                <a class="btn btn-info btn-sm" href="{{ route('datakar.create') }}">Tambah Bagian</a>
            </div>
        </div>
    </div>

    </br>
   
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
   
    <table class="table table-borderes table-striped table-hover">
        <tr>
            <th>NIK</th>
            <th>QR Code</th>
            <th>NAMA</th>
            <th>GENDER</th>
            <th>TTL</th>
            <th>ALAMAT</th>
            <th>TELEPON</th>
            <th>BAGIAN</th>
            <th>TANGGAL JOIN</th>
            <th>AKSI</th>
        </tr>
        @foreach ($karyawan as $kar)
        <tr >
            <td>{{ $kar->karnik }} </td>
            <td>{!! QrCode::size(100)->generate($kar->karnik); !!}</td>
            <td>{{ $kar->karnama }}</td>
            <td>{{ $kar->karjk }}</td>
            <td>{{ $kar->kartl }}</td>
            <td>{{ $kar->karal }}</td>
            <td>{{ $kar->kartelp }}</td>
            <td>{{ $kar->bagian->bagnama }}</td>
            <td>{{ $kar->kartgljoin }}</td>
            <td>
                <form action="{{ route('datakar.destroy', $kar->karnik)}}" method="post">
                <a href="{{ route('datakar.edit', $kar->karnik)}}" class="btn btn-warning btn-sm">Edit</a>
                    @csrf
                    @method('DELETE')      
                    <button type="submit" class="btn btn-danger btn-sm">Delete</button>
                </form>
            </td>
        </tr>
        @endforeach
    </table>  
@endsection

